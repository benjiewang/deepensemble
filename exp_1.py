import argparse
import torch
import torch.optim as optim

import net_utils


def main():
    # Training settings
    parser = argparse.ArgumentParser(description='PyTorch MNIST Example 1')
    parser.add_argument('--batch-size', type=int, default=64, metavar='N',
                        help='input batch size for training (default: 64)')
    parser.add_argument('--test-batch-size', type=int, default=1000, metavar='N',
                        help='input batch size for testing (default: 1000)')
    parser.add_argument('--epochs', type=int, default=10, metavar='N',
                        help='number of epochs to train (default: 10)')
    parser.add_argument('--lr', type=float, default=0.01, metavar='LR',
                        help='learning rate (default: 0.01)')
    parser.add_argument('--momentum', type=float, default=0.5, metavar='M',
                        help='SGD momentum (default: 0.5)')
    parser.add_argument('--no-cuda', action='store_true', default=False,
                        help='disables CUDA training')
    parser.add_argument('--seed', type=int, default=1, metavar='S',
                        help='random seed (default: 1)')
    parser.add_argument('--log-interval', type=int, default=10, metavar='N',
                        help='how many batches to wait before logging training status')

    parser.add_argument('--save-model', action='store_true', default=False,
                        help='For Saving the current Model')
    args = parser.parse_args()
    use_cuda = not args.no_cuda and torch.cuda.is_available()

    torch.manual_seed(args.seed)

    device = torch.device("cuda" if use_cuda else "cpu")

    kwargs = {'num_workers': 1, 'pin_memory': True} if use_cuda else {}

    # Load in-distribution (0-7) and out-of-distribution (8-9) datasets.
    n_classes=8
    train_dataset_in = net_utils.load_mnist(train=True, in_distribution=True, n_classes=n_classes)
    train_dataset_out = net_utils.load_mnist(train=True, in_distribution=False, n_classes=n_classes)
    test_dataset_in = net_utils.load_mnist(train=False, in_distribution=True, n_classes=n_classes)
    test_dataset_out = net_utils.load_mnist(train=False, in_distribution=False, n_classes=n_classes)


    args.epochs= 10
    M = 5  # Number of NNs in ensemble
    models = [net_utils.Net(n_classes).to(device) for i in range(M)]
    optimizers = [optim.SGD(model.parameters(), lr=args.lr, momentum=args.momentum) for model in models]

    # Train NN-ensemble on in-distribution examples and save models
    train_loader = torch.utils.data.DataLoader(
            train_dataset_in, batch_size=args.batch_size, shuffle=True, **kwargs)
    test_loader = torch.utils.data.DataLoader(
        test_dataset_in, batch_size=args.test_batch_size, shuffle=True, **kwargs)
    for i in range(M):
        for epoch in range(1, args.epochs + 1):
            train(args, models[i], device, train_loader, optimizers[i], epoch)
            test(args, models[i], device, test_loader)
    
    
    for i in range(M):
        torch.save(models[i].state_dict(), "mnist_cnn_" + str(i) + ".pt")

        
    # Load saved NN-ensemble
    """
    for i in range(M):
        models[i].load_state_dict(torch.load("mnist_cnn_" + str(i) + ".pt"))
        models[i].eval()
    """
    
    # Test NN-ensemble on unseen in-distribution and out-of-distribution examples.
    test_in_loader = torch.utils.data.DataLoader(
        test_dataset_in, batch_size=args.test_batch_size, shuffle=True, **kwargs)
    in_en = net_utils.calc_entropy(args, models, device, test_in_loader, n_classes)

    test_out_loader = torch.utils.data.DataLoader(
        test_dataset_out, batch_size=args.test_batch_size, shuffle=True, **kwargs)
    out_en = net_utils.calc_entropy(args, models, device, test_out_loader, n_classes)

    print(torch.mean(in_en))
    print(torch.mean(out_en))


if __name__ == '__main__':
    main()