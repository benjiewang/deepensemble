import argparse
import torch
import torch.optim as optim
import numpy as np
from PIL import Image
from torchvision import datasets, transforms
from torch.utils.data.dataset import Dataset
import seaborn as sns
from matplotlib import pyplot as plt

import net_utils


class MNIST_from_data(Dataset):
    """`MNIST <http://yann.lecun.com/exdb/mnist/>`_ Dataset initialized with data directly.
    Args:
        data (Tensor): MNIST data
        true_targets (Tensor): True MNIST targets
        targets (Tensor): May differ from true_targets where an entry is erroneous.
        transform (callable, optional): A function/transform that  takes in an PIL image
            and returns a transformed version. E.g, ``transforms.RandomCrop``
        target_transform (callable, optional): A function/transform that takes in the
            target and transforms it.
    """

    classes = ['0 - zero', '1 - one', '2 - two', '3 - three', '4 - four',
               '5 - five', '6 - six', '7 - seven', '8 - eight', '9 - nine']



    def __init__(self, data, targets, true_targets, transform=None, target_transform=None):
        super(MNIST_from_data, self).__init__()
        self.transform = transform
        self.target_transform = target_transform

        self.data, self.targets, self.true_targets = data, targets, true_targets

    def __getitem__(self, index):
        """
        Args:
            index (int): Index
        Returns:
            tuple: (image, target) where target is index of the target class.
        """
        img, target = self.data[index], int(self.targets[index])

        # doing this so that it is consistent with all other datasets
        # to return a PIL Image
        img = Image.fromarray(img.numpy(), mode='L')

        if self.transform is not None:
            img = self.transform(img)

        if self.target_transform is not None:
            target = self.target_transform(target)

        return img, target

    def __len__(self):
        return len(self.data)


def main():
    # Training settings
    parser = argparse.ArgumentParser(description='PyTorch MNIST Example 2')
    parser.add_argument('--batch-size', type=int, default=64, metavar='N',
                        help='input batch size for training (default: 64)')
    parser.add_argument('--test-batch-size', type=int, default=1000, metavar='N',
                        help='input batch size for testing (default: 1000)')
    parser.add_argument('--epochs', type=int, default=10, metavar='N',
                        help='number of epochs to train (default: 10)')
    parser.add_argument('--lr', type=float, default=0.01, metavar='LR',
                        help='learning rate (default: 0.01)')
    parser.add_argument('--momentum', type=float, default=0.5, metavar='M',
                        help='SGD momentum (default: 0.5)')
    parser.add_argument('--no-cuda', action='store_true', default=False,
                        help='disables CUDA training')
    parser.add_argument('--seed', type=int, default=1, metavar='S',
                        help='random seed (default: 1)')
    parser.add_argument('--log-interval', type=int, default=10, metavar='N',
                        help='how many batches to wait before logging training status')

    parser.add_argument('--save-model', action='store_true', default=False,
                        help='For Saving the current Model')
    args = parser.parse_args()
    use_cuda = not args.no_cuda and torch.cuda.is_available()

    torch.manual_seed(args.seed)

    device = torch.device("cuda" if use_cuda else "cpu")

    kwargs = {'num_workers': 1, 'pin_memory': True} if use_cuda else {}


    # Load datasets
    dataset = net_utils.load_mnist(train=True, in_distribution=True, n_classes=10)
    n_errors = len(dataset) // 100
    error_locs = np.random.choice(len(dataset), n_errors, replace=False) # Random locations of poisoned examples
    # Add a number random between 1 and 9 inclusive mod 10 to the label to ensure poisoned label is different
    error_diffs = np.random.choice(np.arange(1, 10), n_errors, replace=True).astype(np.int64)

    new_data = dataset.train_data
    new_true_targets = dataset.train_labels.clone() # True labels
    new_targets = dataset.train_labels.clone()  # Poisoned labels
    new_targets[error_locs] = torch.remainder(new_targets[error_locs] +
                                              torch.from_numpy(error_diffs), 10)


    K = 3  # Number of folds
    split_locs = np.arange(K + 1) * len(dataset) // K

    args.epochs = 5
    M = 5  # Number of NNs in ensemble

    # Tensors with length equal to the dataset size, to be filled up fold-by-fold
    entropies = torch.Tensor()
    predictions = torch.LongTensor()

    for k in range(K):
        new_train_data = torch.cat((new_data[np.arange(split_locs[0], split_locs[k])],
                                    new_data[np.arange(split_locs[k + 1], split_locs[K])]))
        new_train_targets = torch.cat((new_targets[np.arange(split_locs[0], split_locs[k])],
                                       new_targets[np.arange(split_locs[k + 1], split_locs[K])]))
        new_train_true_targets = torch.cat((new_true_targets[np.arange(split_locs[0], split_locs[k])],
                                             new_true_targets[np.arange(split_locs[k + 1], split_locs[K])]))


        new_test_data = new_data[np.arange(split_locs[k], split_locs[k + 1])]
        new_test_targets = new_targets[np.arange(split_locs[k], split_locs[k + 1])]
        new_test_true_targets = new_true_targets[np.arange(split_locs[k], split_locs[k + 1])]

        new_train_dataset = MNIST_from_data(data=new_train_data, targets=new_train_targets,
                                            true_targets=new_train_true_targets, transform=transforms.Compose([
            transforms.ToTensor(),
            transforms.Normalize((0.1307,), (0.3081,))
        ]))

        new_test_dataset = MNIST_from_data(data=new_test_data, targets=new_test_targets, true_targets=new_test_true_targets,
                                            transform=transforms.Compose([
                                                transforms.ToTensor(),
                                                transforms.Normalize((0.1307,), (0.3081,))
                                            ]))



        models = [net_utils.Net(10).to(device) for i in range(M)]
        optimizers = [optim.SGD(model.parameters(), lr=args.lr, momentum=args.momentum) for model in models]

        train_loader = torch.utils.data.DataLoader(
            new_train_dataset, batch_size=args.batch_size, shuffle=True, **kwargs)
        test_loader = torch.utils.data.DataLoader(
            new_test_dataset, batch_size=args.test_batch_size, shuffle=False, **kwargs) #False to check predictions

        for i in range(M):
            for epoch in range(1, args.epochs + 1):
                net_utils.train(args, models[i], device, train_loader, optimizers[i], epoch)
                net_utils.test(args, models[i], device, test_loader)


        for i in range(M):
            torch.save(models[i].state_dict(), "mnist_cnn_fold" + str(k) + "_model" + str(i) + ".pt")

        entropies = torch.cat((entropies, net_utils.calc_entropy(args, models, device, test_loader, 10)))
        predictions = torch.cat((predictions, net_utils.calc_predictions(args, models, device, test_loader, 10)))

    is_poisoned = np.full(shape=len(dataset), fill_value=False, dtype=bool)
    is_poisoned[error_locs] = True
    print(is_poisoned)

    entropy_limits = np.append(np.arange(1, 21) / 20, 9999)
    precisions = np.empty(21)
    recalls = np.empty(21)
    for idx, entropy_limit in enumerate(entropy_limits):
        is_pred_poisoned = ((predictions != new_targets) * (entropies < entropy_limit)).numpy()
        TP = np.sum(is_poisoned & is_pred_poisoned)
        FP = np.sum(np.logical_not(is_poisoned) & is_pred_poisoned)
        FN = np.sum(is_poisoned & np.logical_not(is_pred_poisoned))
        precision = TP / (TP + FP)
        recall = TP / (TP + FN)
        precisions[idx] = precision
        recalls[idx] = recall

    sns.set()
    sns.lineplot(recalls, precisions)
    plt.xlabel("Recall")
    plt.ylabel("Precision")
    plt.xlim((0, 1))
    plt.ylim((0, 1))
    plt.show()


if __name__ == '__main__':
    main()
