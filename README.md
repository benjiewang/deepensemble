# Deep Ensembles

Code to accompany Paper Assignment for application to DeepBayes summer school.

### Prerequisites

Following Python packages are required:

* numpy
* PyTorch
* PIL
* sklearn
* pyplot, seaborn


## Experiments

Please see the accompanying report. This details the purpose of each experiment.


## Authors

* **Benjie Wang**
