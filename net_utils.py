import torch
import torch.nn as nn
import torch.nn.functional as F
from torchvision import datasets, transforms


class Net(nn.Module):
    """Standard ConvNet Architecture for MNIST.
    Args:
        n_classes (int): Indicates how many classes to target. 10 means all 10 digits 0-9.
    """
    def __init__(self, n_classes):
        super(Net, self).__init__()
        self.conv1 = nn.Conv2d(1, 20, 5, 1)
        self.conv2 = nn.Conv2d(20, 50, 5, 1)
        self.fc1 = nn.Linear(4 * 4 * 50, 500)
        self.fc2 = nn.Linear(500, n_classes)

    def forward(self, x):
        x = F.relu(self.conv1(x))
        x = F.max_pool2d(x, 2, 2)
        x = F.relu(self.conv2(x))
        x = F.max_pool2d(x, 2, 2)
        x = x.view(-1, 4 * 4 * 50)
        x = F.relu(self.fc1(x))
        x = self.fc2(x)
        return F.log_softmax(x, dim=1)


def train(args, model, device, train_loader, optimizer, epoch):
    model.train()
    for batch_idx, (data, target) in enumerate(train_loader):
        data, target = data.to(device), target.to(device)
        optimizer.zero_grad()
        output = model(data)
        loss = F.nll_loss(output, target)
        loss.backward()
        optimizer.step()
        if batch_idx % args.log_interval == 0:
            print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
                epoch, batch_idx * len(data), len(train_loader.dataset),
                       100. * batch_idx / len(train_loader), loss.item()))


def test(args, model, device, test_loader):
    model.eval()
    test_loss = 0
    correct = 0
    with torch.no_grad():
        for data, target in test_loader:
            data, target = data.to(device), target.to(device)
            output = model(data)
            test_loss += F.nll_loss(output, target, reduction='sum').item()  # sum up batch loss
            pred = output.argmax(dim=1, keepdim=True)  # get the index of the max log-probability
            correct += pred.eq(target.view_as(pred)).sum().item()

    test_loss /= len(test_loader.dataset)

    print('\nTest set: Average loss: {:.4f}, Accuracy: {}/{} ({:.0f}%)\n'.format(
        test_loss, correct, len(test_loader.dataset),
        100. * correct / len(test_loader.dataset)))


def load_mnist(train=True, in_distribution=True, n_classes=8):
    """
    Args:
        train (bool): Whether to load MNIST training or test data
        n_classes (int): The number of classes that are in-distribution (cf Section 3.1).
            0 to (n_classes - 1) inclusive are considered in-distribution
        in_distribution (bool): Whether to load in-distribution or out-of-distribution data
    Returns:
        dataset (Dataset): A MNIST dataset containing the appropriate examples.
    """
    dataset = datasets.MNIST('../data', train=train, download=True, transform=transforms.Compose([
        transforms.ToTensor(),
        transforms.Normalize((0.1307,), (0.3081,))
    ]))
    if train:
        idx = (dataset.train_labels < n_classes) if in_distribution else (dataset.train_labels >= n_classes)
        dataset.train_labels = dataset.train_labels[idx]
        dataset.train_data = dataset.train_data[idx]
    else:
        idx = (dataset.test_labels < n_classes) if in_distribution else (dataset.test_labels >= n_classes)
        dataset.test_labels = dataset.test_labels[idx]
        dataset.test_data = dataset.test_data[idx]

    return dataset


def calc_entropy(args, models, device, test_loader, n_classes):
    """ Calculates entropy based on the NN ensemble specified by models on each
        example in test_loader.
    """
    entropies = []

    with torch.no_grad():
        for data, target in test_loader:
            total_prob = torch.zeros([data.size()[0], n_classes])
            for model in models:
                output = model(data)
                prob = torch.exp(output)
                total_prob += prob
            avg_prob = total_prob/len(models)


            entropy = -torch.sum(avg_prob * torch.log(avg_prob), dim=1)
            entropies.append(entropy)

    return torch.cat(entropies)


def calc_predictions(args, models, device, test_loader, n_classes):
    """ Calculates predictions based on the NN ensemble specified by models on each
        example in test_loader.
    """
    predictions = []

    with torch.no_grad():
        for data, target in test_loader:
            total_prob = torch.zeros([data.size()[0], n_classes])
            for model in models:
                output = model(data)
                prob = torch.exp(output)
                total_prob += prob
            avg_prob = total_prob / len(models)

            _, prediction = torch.max(avg_prob, 1)
            predictions.append(prediction)

    return torch.cat(predictions)